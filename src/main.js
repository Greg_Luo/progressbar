import { createApp } from "vue";
import App from "./App.vue";
import "bootstrap/scss/bootstrap.scss";
import "bootstrap/scss/bootstrap-grid.scss";
import "bootstrap/scss/bootstrap-reboot.scss";
import "bootstrap/scss/bootstrap-utilities.scss";

createApp(App).mount("#app");
