import { shallowMount } from "@vue/test-utils";
import ProgressBar from "@/components/progressBar.vue";

describe("progressBar.vue", () => {
  it("renders props.msg when passed", () => {
    const bar = 50;
    const limit = 100;
    const wrapper = shallowMount(ProgressBar, {
      props: { bar, limit },
    });
    expect(wrapper.text()).toMatch("50%");
  });
});
